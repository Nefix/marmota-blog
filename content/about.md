---
title: About
date: 2019-02-10T16:39:25+01:00
draft: false
author: Néfix Estrada
---

> Marmota is a distributed and Free Software alternative to Spotify (and by extension the majority of the music streaming services)

For more information, see [Weekly 1 - 2019/02/10](../posts/weekly-1)

Follow Marmota on Mastodon: [@marmota@fosstodon.org](https://fosstodon.org/@marmota)  
Follow Néfix Estrada on Mastodon: [@nefix@mastodon.social](https://mastodon.social/@nefix)
