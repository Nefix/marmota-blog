---
title: "Weekly 5 - 2019/03/10"
date: 2019-03-11T13:11:04+01:00
draft: false
cover: https://upload.wikimedia.org/wikipedia/commons/2/26/Marmota_marmota_Alpes2.jpg
author: Néfix Estrada
---

Hello!

It's been a long time since the last update! Last week I didn't post an entry and this one is a day late (*note*: two days late actually).

[Here](https://fosstodon.org/@marmota/101696822143117985) I explain why I didn't write a post last week. *TLDR*: I didn't had time. This week post is late because the same reason. It's being really hard for me to find time for the project (or time for myself in general). I'm finding myself using all the spare time I cand find to sleep. This last two weeks have been really exausting :/

## Work

What have I done?

- Added tests to the majority of the packages and restructurated the directories
- Added Gitlab CI pipelines (build and test)
- Fixed issue [#1](https://gitlab.com/Nefix/marmota/issues/1)
- Created the User model
- Started with the Playlist model

It may seem it's a lot, but it isn't in reallity.

## Plans for this week

This week I'm planning to do:

- Finish the Playlist model
- Start with the web client (Marmota is going to have different clients, one of which is the web client)
- ... ?

Let's see how this week comes out! See ya!