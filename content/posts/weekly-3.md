---
title: "Weekly 3 - 2019/02/24"
date: 2019-02-24T23:44:52+01:00
draft: false
cover: https://upload.wikimedia.org/wikipedia/commons/c/ca/Marmota_monax_UL_02.jpg
author: Néfix Estrada
---

Hello again!

I'm glad you're reading this blog post! This week has been a bit tough for me.

**Note**: this is going to be a really concise blog post, I'm tired, I haven't progressed that much and I need to go to sleep!

### The bad things

I started the week with two issues: I wasn't feeling well and I had really low motivation. I tried to spend time doing things (not just working on Marmota), but I couldn't find a way to be more than 5 minutes focused. I wasted two or three days with this situation.

It was then when, while working, I found *the* reason why I was like that: **Vim**. I realised that most of the time I spent coding I was fighting against the editor and not programing (e.g. doing identation, waiting for code completion...). I discovered it because I had to do some things with VSCode for work and realised how pleasant was the experience.

### The good things

After changing from Vim to [VSCodium](https://github.com/VSCodium/vscodium) (Free/Libre Open Source Software Binaries of VSCode) [using the Vim emulator plugin], I started to feel better (both physically and mentally) and started to code again gradually (and enjoying a lot the time)

### The actual work

Last week I planned to:

- Finish the upload function
- Add tests to the packages that currently don't have
- Start with the ActivityPub implementation
- Fix issue [#1](https://gitlab.com/Nefix/marmota/issues/1)

What I did was merging the first two tasks into one and try solving it. The task is nearly finished so, probably tomorrow I'm going to push the changes to [develop](https://gitlab.com/Nefix/marmota/issues/1) branch.

It has taken me *that* long because I really think that tests are almost as important as the code itself so, I'm trying to get coverage for as much as possible code.

What I plan to do for the next week is:

- Finish the upload function and add tests to all the packages (almost finished)
- Fix issue [#1](https://gitlab.com/Nefix/marmota/tree/develop)
- Setup GitLab CI
- Start with the ActivityPub implementation

Since Marmota has tests now, setting up the GitLab CI is going to ensure that the code pushed to the repository is always working!

### Closing up

Afer having this weird week, I've realised one thing: the development of Marmota is going to be slower that what I'd like. I thought I could spend more time coding and that I'd always want to spend my precious time writing Marmota. This week has helped me to understand that this is not true and to rediscover my limits.

But now that I've tasted the bitter reallity, I'm better prepared and ready for another exciting week!

Have a nice one!