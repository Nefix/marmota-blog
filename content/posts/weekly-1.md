---
title: Weekly 1 - 2019/02/10
date: 2019-02-10T11:09:16+01:00
draft: false
cover: https://upload.wikimedia.org/wikipedia/commons/3/3b/Marmot-edit1.jpg
author: Néfix Estrada
---

```go
package main

import "fmt"

func main() {
	fmt.Println("Hello, World! This is the first blog entry of Marmota's blog!")
}

```

In this first blog entry I'm going to talk about what Marmota is, the design behind it, the reasons why I'm starting it, and many more!

### What is Marmota?

As the description in the GitLab page states:

> Marmota is a distributed and Free Software alternative to Spotify (and by extension the majority of the music streaming services)

But what does that actually mean?

Basically, Marmota is going to be a service like Spotify (or any music streaming service like it) but is going to be free software (check the [LICENSE](https://gitlab.com/Nefix/marmota/blob/master/LICENSE)).

By distributed, I mean **really**  distributed. It's going to use [IPFS](https://ipfs.io/) underneath so, if a server dies, the server library is still going to be accessible. This is the key difference between Marmota and [Funkwhale](https://join.funkwhale.audio/).

By using IPFS, the content delivery is going to be faster and the servers are going to have less load, since the actual music is going to be sent (in part) by other peers.

Also, Marmota is going to be part of the [Fediverse](https://en.wikipedia.org/wiki/Fediverse), using the ActivityPub standard. This is going to enable Marmota to comunicate not just with other services like it such as Funkwhale, but with the whole network!

### Design

When I started designing how Marmota was going to be, I wrote some ideas I had and drew a some sketches about how I wanted Marmota to work. This is the result of my terrible handwritting, my terrible draw skills and a week inside my bag.

{{< image src="/img/weekly-1/sketches-1.jpg" alt="Sketch 1. Really bad handwritting." position="center" style="border-radius: 8px;" >}}
{{< image src="/img/weekly-1/sketches-2.jpg" alt="Sketch 2. Really bad handwritting." position="center" style="border-radius: 8px;" >}}

In the end it turned quite similar. Marmota is going to work really tight with MusicBrainz. It's going to use it for almost everything: metadata, ID's, cover arts... This has a really simple reason: the MusicBrainz has all the information Marmota needs and it's always going to be updated. If you want to change some Marmota's metadata, you can change it in the MusicBrainz DB. You are going to enjoy these changes and the whole community is going to do so!

#### How is it going to work?

1. Client does a search
2. Marmota's server queries it in the MusicBrainz DB
3. The server returns the results to the client
4. Client selects an item. The selection is a MusicBrainz ID.
5. The server queries it's own relay for all the different stream options (different files)
6. Marmota selects the best option (see [Best Option Selection](#best-option-selection))
7. The server returns the IPFS URL of the file
8. The client enjoys the song!

#### Relay?

Yes. The server is going to maintain a local DB with entries from all the other Marmota's servers (see [Database Design](#database-design)). This is going to reduce **a lot** the calls between servers and, if server disappears, the rest of the network is going to be able to access to all it's library, without noticing it.

The relay is going to be updated every time there's a change in another server: when server A updates it's library, it's going to notify to all the servers (for example B, C and D) that it has done changes in the DB. Also, the relay is going to update the library of each server every X time (I'm not yet sure about how much time it should be) in case it missed an update.

#### Best Option Selection

The Best Option Selection is going to be an algorithm that is going to select the best file for the user depending on the following variables:

- Song quality (the bitrate)
- Rating (see [Rating](#rating))
- Client connection speed
- Client connection type (not worth downloading 300MB of a FLAC with a limit cap)
- Client preferences

#### Discovery

The discovery is a crytical part of Marmota, since the whole point is to have "an universal library". When a server is created, it's going to ask for an already running Marmota server. Then, new server is going to say to the server the user has introduced: "hey, I'm new here, could you please introduce me to your friends?"

Basically, the server that the user has introduced is going to return it's list of known servers and notify each one that there's a new server in the network. Also, every X time it's going to select 2 or 3 random servers in it's known servers list and sync it's list.

#### Database Design

This section is only going to talk about the library part of Marmota and the discovered servers. The tables are going to be as it follows:

| IPFS URL (PK) | MusicBrainz ID | Server URL | Rating | Bitrate |
| ------------- | -------------- | ---------- | ------ | ------- |
| The IPFS URL to the actual file | The MusicBrainz song ID | The URL of the server where it was uploaded | The rating of the song (see [Rating](#rating)) | The bitrate of the file |

| Server URL | Last Updated | Blocked |
| ---------- | ------------ | ------- |
| The URL of the server | The time where the relay data of this server was last updated | If the instance is blocked or not |

#### Rating

The songs are going to have a rating. This rating does not represent if the user likes the song. It's going to represent the song accuracy. This is done because a user could upload a song and link it with an incorrect MusicBrainz ID. This should prevent having a bad library


#### Playlists

Users are going to be able to create playlists. The playlists are going to store the MusicBrainz ID. This enables the user to change their preferences and have those changes reflected to all their songs.

#### Status

This feature, that is going to be disabled by default, is going to publish the song that you are currently listening. Your followers are going to be able to see what you are listening. This is the same thing as the Spotify friends feed.


#### Radios

Using the status feature, people is going to be able to "sync" with other users status. This is going to produce the same effect as a live radio (when the user that you are "syncing" with changes of song, you are going to change also)


### Reasons

It's been a long time since I had the project idea (or something like that). In fact, it was when I started learning to code that I wanted to build a music library. This wasn't just because of the pragmatism of having your local music library available from where you want, but because my way of understanding the world and my thoughts about culture.

One of my first attempts was called "Musik Liberation" and had this text as description:

> Currently, music is a business. The big music enterprises always want to make more and more and more money. That's why you can't  download or listen music without paying or listening annoying ads. Hate those people that just make music to win money. Music is a way of expressing yourself, not a way to make business. That's why I've started with Musik Liberation: to free the music. Everyone should be able to listen and enjoy music without having a big enterprise trading with their data or saying them what to listen and how to do it. Listen to what you want and how you want!

Because of my lacking programming skills, I abandoned this project, but I always had the idea of building something similar. Half year ago, I started again with the project, but that was when I discovered the Fediverse. It changed my way of understanding how the internet *could* work and how I wanted to take part of it. It was then when I started to think about what I wanted Marmota to be and what I didn't.

> The process of federation and decentralization is a political act and a real goal for me and one of the reasons I got involved in standarizing things is about distributing power.

[Christopher Lemmer Webber](https://octodon.social/@cwebber), 2019-02-02

The quote was extracted from the ActivityPub panel at Fosdem 2019: [https://peertube.social/videos/watch/c7e0d798-9311-4bc5-93c5-87f452aa558a?start=4m37s](https://peertube.social/videos/watch/c7e0d798-9311-4bc5-93c5-87f452aa558a?start=4m37s)

Piratea, difunde y comparte!

### Name

Marmota is an animal. Quoting from the song "Generemos Kaos - Marmotas en el Bar" (Let's generate chaos)

> Marmots are an animal with a community character that is organized without power relations in its social structure. Females and males take care of their child without wanting to dominate each other. From this point of view, we could say that marmots are in favor of anarchy

Also, they are really cute!

### Actual work this week

This week hasn't been *that* productive, since I was really tired because of Fosdem. I've been working on the design. This took me the majority of time / energy, since I really wanted to have a strong base to start with the project. I've also started with the MusicBrainz API client. Also, I've created Marmota's blog and written this article.

### Development Model

I want to build a community around Marmota and let the community manage the project. But before that I want to build a good base for the project. Meanwhile, I'm going to be managing the project but when the time comes, I'm going to step down and let the community choose the direction of the project. Any kind of authority is bad, even the [Benevolent Dictator](http://oss-watch.ac.uk/resources/benevolentdictatorgovernancemodel)

### Feedback

I'd really love to have some feedback about what things could be improved, are bad designed, ideas or just some interest to the project!

### Thanks

Huge thanks to [@eliotberriot@mastodon.eliotberriot.com](https://mastodon.eliotberriot.com/@eliotberriot), [@cj@mastodon.technology](https://mastodon.technology/@cj) and all the other people who helped me with ideas, giving advice or just encouraging me to start with this great project!

**PS**:

The source of the blog is available [here](https://gitlab.com/nefix/marmota-blog). The blog is hosted using IPFS :D
